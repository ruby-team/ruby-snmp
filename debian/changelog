ruby-snmp (1.3.2-2) UNRELEASED; urgency=medium

  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.0, no changes needed.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 01 Sep 2022 18:16:16 -0000

ruby-snmp (1.3.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bumped Standards-Version to 4.5.0.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sat, 05 Dec 2020 07:21:00 +0900

ruby-snmp (1.3.1-2) unstable; urgency=medium

  [ Nobuhiro Iwamatsu ]
  * debian/control:
    Update Homepage field. Change from rubyforge.org to github.com.
  * debian/copyright:
    Update Source field. Change from rubyforge.org to github.com.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 07:43:56 +0530

ruby-snmp (1.3.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bumped Standards-Version to 4.2.1.
    - Updated Vcs-Git and Vcs-Browser to using salsa.
    - Bumped version of debhelper to 10.
    - Remove Replaces, Conflicts and Provides.
  * debian/watch:
    Fix to use gemwatch.debian.net.
  * debian/compat
    Bumped to 10.
  * debian/copyright:
    Updated to DEP5 format.
  * debian/rules
    Fix permisson of ruby files under usr/share/doc/.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Thu, 25 Oct 2018 16:57:52 +0900

ruby-snmp (1.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.5.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 29 Jul 2014 02:47:24 +0900

ruby-snmp (1.1.1-2) unstable; urgency=low

  * Fix upgrade from wheezy.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Thu, 12 Sep 2013 09:48:46 +0900

ruby-snmp (1.1.1-1) unstable; urgency=low

  * New upstream release.
  * Update debian/rules.
    - Add override_dh_compress in debian/rules
	  ignore .yaml in dh_compress.
  * Update debian/control.
    - Build depend on gem2deb >= 0.3.0.
    - Drop transitional packages and ruby1.8.
    - Add Vcs-Git and Vcs-Browser field.
    - Set Architecture to all.
  * Update patches/avoid_diverting_mibs.diff
  * Remove debian/docs

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 10 Sep 2013 04:06:19 +0900

ruby-snmp (1.0.2-2) experimental; urgency=low

  * New maintainer. (Closes: #677773)
  * Bump Standards-Version to 3.9.4.
  * Switch to dh.
  * Switch to gem2deb-based packaging. Rename source and binary packages.
  * Remove DM-Upload-Allowed field.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Wed, 10 Oct 2012 16:07:25 +0900

libsnmp-ruby (1.0.2-1) unstable; urgency=low

  * New upstream release.
  * Switch from dpatch to quilt.
    + Create avoid_diverting_mibs.diff by refreshing old
      01_avoid_diverting_mibs.dpatch.
  * Bump Standards-Version to 3.7.3.
  * Add the Homepage field and remove it from the long description.
  * Add the Vcs-Git/Vcs-Browser fields.
  * Add the DM-Upload-Allowed field.

 -- Aurélien GÉRÔME <ag@roxor.cx>  Thu, 07 Feb 2008 14:03:05 +0100

libsnmp-ruby (1.0.1-1) unstable; urgency=low

  * New maintainer. (Closes: #387550)
  * New upstream release.
  * Update Standards-Version to 3.7.2.
  * Update package description with project homepage.
  * Use architecture all instead of architecture any. (Closes: #389013)
  * Add debian/watch file.
  * Convert to dpatch.

 -- Aurélien GÉRÔME <ag@roxor.cx>  Sun, 15 Oct 2006 19:50:02 +0200

libsnmp-ruby (0.4.1-3) unstable; urgency=low

  * Avoid diverting MIBs.
  	- Thanks to Brandon Hale <brandon@ubuntu.com>.

 -- David Moreno Garza <damog@debian.org>  Fri, 21 Oct 2005 00:45:22 -0500

libsnmp-ruby (0.4.1-2) unstable; urgency=low

  * Changed maintainer's address.
  * Updated policy compliant version.

 -- David Moreno Garza <damog@debian.org>  Fri,  2 Sep 2005 13:09:33 -0500

libsnmp-ruby (0.4.1-1) unstable; urgency=low

  * Initial release (Closes: #311003).

 -- David Moreno Garza <damog@damog.net>  Sun, 29 May 2005 15:49:02 -0500
